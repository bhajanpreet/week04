
public class StringExamples1 {
	public static void main(String[] args) {
		String name = "Bhajanpreet";
		System.out.println(name);
		
		System.out.println(name.length());
		
		char charAtPos4 = name.charAt(4);
		System.out.println(charAtPos4);
		
		String sub = name.substring(0, 5);
		System.out.println(sub);
		
		String sub1 = name.substring(2);
		System.out.println(sub1);
		
		
	}
}
